function [ c ] = LZ78(X)
%%LZ78 Compute LZ78 complexity of univariate time signal X using a standard
% word-dictionary algorithm. To generate a discrete sequence from a real-valued
% X, X is detrended and binarised with a threshold of 0.
%
% Inputs:
%   X -- 1D array with real-valued signal
%
% Outputs:
%   c -- raw (unnormalised) value of LZ78 complexity (i.e. dictionary length)
%
% Pedro Mediano, Oct 2019 (based on an implementation by Lionel Barnett)

%% Argument checks
if ndims(X) > 2 || ~any(size(X) == 1)
  error('Input array must be 1D');
end


%% Detrend signal and binarise into char string
v = detrend(X(:)') > 0;
s = strrep(num2str(v), ' ', '');


%% Main LZ78 algorithm, add substrings to dictionary and return its length
dict = containers.Map;      % the dictionary
w = [];                     % initialise current word
for ch = s                  % iterate through input characters
  w = [w ch];             % append current character to current word
  if ~isKey(dict,w)       % if current word not already in dictionary...
    dict(w) = true;     % add to dictionary
    w = [];             % and reinitialise
  end
end
c = length(dict);           % LZ complexity is size of dictionary

